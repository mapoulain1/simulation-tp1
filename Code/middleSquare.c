/**
 * @file middleSquare.h
 * @author Maxime POULAIN
 * @date 2021-09-13
 * 
 */

#include "middleSquare.h"

int internalMiddleSquareSeed;

/**
 * @brief Set the seed for the middle square generator
 * 
 * @param seed 
 */
void middleSquareSeed(int seed) {
	internalMiddleSquareSeed = seed;
}

/**
 * @brief Return random a value with the middle square implementation
 * 
 * @return Random
 */
int middleSquare(void) {
	internalMiddleSquareSeed = (internalMiddleSquareSeed * internalMiddleSquareSeed / 100) % 10000;
	return internalMiddleSquareSeed;
}
