/**
 * @file color.h
 * @author Maxime POULAIN
 * @brief 
 * @date 2018-10-13
 * 
 */

#ifndef COLOR_H
#define COLOR_H

#define RED "\x1b[31m"
#define GRE "\x1b[32m"
#define YEL "\x1b[33m"
#define BLU "\x1b[34m"
#define MAG "\x1b[35m"
#define CYA "\x1b[36m"
#define GRY "\x1b[30m"
#define WHY "\x1b[37m"

#define RESET "\x1b[0m"

#endif
