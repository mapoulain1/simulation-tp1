/**
 * @file TP1.c
 * @author Maxime POULAIN
 * @date 2021-09-13
 * 
 */

#include "TP1.h"
#include "color.h"

#include "lcg.h"
#include "middleSquare.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/**
 * @brief Roll a custom dice N times
 * 
 * @param faces Number of faces of the dice
 * @param numberOfTry Number of times to roll
 */
void rollTheDice(int faces, int numberOfTry) {
	int *results = malloc(sizeof(int) * faces);
	if (!results)
		return;

	for (int i = 0; i < faces; i++) {
		results[i] = 0;
	}

	for (int i = 0; i < numberOfTry; i++) {
		results[rand() % faces]++;
	}

	printf("Distribution on %d tries %d faces : ", numberOfTry, faces);
	for (int i = 0; i < faces; i++) {
		printf("%4.01f%%\t", ((float)results[i] / numberOfTry) * 100);
	}
	printf("\n");
	free(results);
}

/**
 * @brief Toss a coin N times
 * 
 * @param numberOfTry Number of times to toss
 */
void tossingCoin(int numberOfTry) {
	int results[2] = {0, 0};

	for (int i = 0; i < numberOfTry; i++) {
		results[rand() % 2]++;
	}
	printf("Distribution on %d tries: %4.01f%% %4.01f%%\n", numberOfTry, ((float)results[0] / numberOfTry) * 100, ((float)results[1] / numberOfTry) * 100);
}

void testMiddleSqure(void) {
	printf(RED "\n\n\nTEST MIDDLE SQUARE\n" RESET);
	printf(GRE "SEED : 1234\n" RESET);
	middleSquareSeed(1234);
	for (int i = 0; i < 60; i++) {
		printf("Iteration %02d : %04d\n", i, middleSquare());
	}

	printf(GRE "SEED : 4100\n" RESET);
	middleSquareSeed(4100);
	for (int i = 0; i < 10; i++) {
		printf("Iteration %02d : %04d\n", i, middleSquare());
	}

	printf(GRE "SEED : 3141\n" RESET);
	middleSquareSeed(3141);
	for (int i = 0; i < 10; i++) {
		printf("Iteration %02d : %04d\n", i, middleSquare());
	}
}

void testTossingCoin(void) {
	printf(RED "\nTEST TOSS A COIN (to your witcher...)\n" RESET);
	srand(10);
	tossingCoin(10);
	tossingCoin(100);
	tossingCoin(1000);
	tossingCoin(1000000);
}

void testRollingDice(void) {
	printf(RED "\n\n\nTEST ROLL THE DICE\n" RESET);
	srand(10);
	rollTheDice(6, 10);
	rollTheDice(6, 100);
	rollTheDice(6, 1000);
	rollTheDice(6, 1000000);

	printf("\n\n");

	rollTheDice(10, 10);
	rollTheDice(10, 100);
	rollTheDice(10, 1000);
	rollTheDice(10, 1000000);
}
void testLCG(void) {
	printf(RED "\n\n\nTEST LCG\n" RESET);
	LCGSeed(5, 5, 1, 16);
	printf(GRE "SEED : %d\n" RESET, LCGGetSeed());
	printf(GRE "A : %d\n" RESET, LCGGetA());
	printf(GRE "C : %d\n" RESET, LCGGetC());
	printf(GRE "M : %d\n" RESET, LCGGetMod());
	for (int i = 0; i < 60; i++) {
		printf("Iteration %02d : %3d\n", i, LCG());
	}
}

void testFloatRand(void) {
	printf(RED "\n\n\nTEST LCG FLOAT\n" RESET);
	LCGSeed(5, 5, 1, 16);
	printf(GRE "SEED : %d\n" RESET, LCGGetSeed());
	printf(GRE "A : %d\n" RESET, LCGGetA());
	printf(GRE "C : %d\n" RESET, LCGGetC());
	printf(GRE "M : %d\n" RESET, LCGGetMod());
	for (int i = 0; i < 10; i++) {
		printf("%.4f\n", floatRand());
	}
	printf("\n");

	LCGSeed(500, 404, 19, 7);
	printf(GRE "BAD SEED : %d\n" RESET, LCGGetSeed());
	printf(GRE "BAD A : %d\n" RESET, LCGGetA());
	printf(GRE "BAD C : %d\n" RESET, LCGGetC());
	printf(GRE "BAD M : %d\n" RESET, LCGGetMod());
	for (int i = 0; i < 10; i++) {
		printf("%.4f\n", floatRand());
	}

	//m prime, c = 0
	LCGSeed(12, 7, 0, 97);
	printf(GRE "OPTIMIZED SEED : %d\n" RESET, LCGGetSeed());
	printf(GRE "OPTIMIZED A : %d\n" RESET, LCGGetA());
	printf(GRE "OPTIMIZED C : %d\n" RESET, LCGGetC());
	printf(GRE "OPTIMIZED M : %d\n" RESET, LCGGetMod());
	for (int i = 0; i < 10; i++) {
		printf("%.4f\n", floatRand());
	}
}

int main(int argc, char const *argv[]) {
	testMiddleSqure();
	testTossingCoin();
	testRollingDice();
	testLCG();
	testFloatRand();

	return 0;
}
