/**
 * @file middleSquare.h
 * @author Maxime POULAIN
 * @date 2021-09-13
 * 
 */

#ifndef MIDDLESQUARE_H_
#define MIDDLESQUARE_H_

/**
 * @brief Set the seed for the middle square generator
 * 
 * @param seed 
 */
void middleSquareSeed(int seed);

/**
 * @brief Return random a value with the middle square implementation
 * 
 * @return Random
 */
int middleSquare(void);

#endif