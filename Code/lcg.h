/**
 * @file lcg.h
 * @author Maxime POULAIN
 * @date 2021-09-13
 *
 */

#ifndef LCG_H_
#define LCG_H_

/**
 * @brief Set the seed for the LCG generator
 * 
 * @param seed The seed
 * @param a Coefficient 
 * @param c C
 * @param m Mod
 */
void LCGSeed(int seed, int a, int c, int m);

/**
 * @brief Return random a value with the LCG
 * 
 * @return Random int
 */
int LCG(void);

/**
 * @brief Interface for LCG
 * 
 * @return Random int
 */
int intRand(void);

/**
 * @brief Interface for LCG for floating point
 * 
 * @return Random float 
 */
float floatRand(void);

/**
 * @brief Getter for the seed
 * 
 * @return Seed
 */
int LCGGetSeed(void);

/**
 * @brief Getter for the coefficient
 * 
 * @return Coefficient
 */
int LCGGetA(void);

/**
 * @brief Getter for the C parameter
 * 
 * @return C parameter
 */
int LCGGetC(void);

/**
 * @brief Getter for the mod
 * 
 * @return Mod
 */
int LCGGetMod(void);

#endif