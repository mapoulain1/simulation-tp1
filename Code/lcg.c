/**
 * @file lcg.h
 * @author Maxime POULAIN
 * @date 2021-09-13
 *
 */

#include "lcg.h"

int internalLCG_Seed;
int internalLCG_A;
int internalLCG_C;
int internalLCG_M;

/**
 * @brief Set the seed for the LCG generator
 * 
 * @param seed The seed
 * @param a Coefficient 
 * @param c C
 * @param m Mod
 */
void LCGSeed(int seed, int a, int c, int m) {
	internalLCG_Seed = seed;
	internalLCG_A = a;
	internalLCG_C = c;
	internalLCG_M = m;
}

/**
 * @brief Return random a value with the LCG
 * 
 * @return Random int
 */
int LCG(void) {
	internalLCG_Seed = (internalLCG_A * internalLCG_Seed + internalLCG_C) % internalLCG_M;
	return internalLCG_Seed;
}

/**
 * @brief Interface for LCG
 * 
 * @return Random int
 */
int intRand(void) {
	return LCG();
}

/**
 * @brief Interface for LCG for floating point
 * 
 * @return Random float 
 */
float floatRand(void) {
	return intRand() / (float)internalLCG_M;
}

/**
 * @brief Getter for the seed
 * 
 * @return Seed
 */
int LCGGetSeed(void) {
	return internalLCG_Seed;
}

/**
 * @brief Getter for the coefficient
 * 
 * @return Coefficient
 */
int LCGGetA(void) {
	return internalLCG_A;
}

/**
 * @brief Getter for the C parameter
 * 
 * @return C parameter
 */
int LCGGetC(void) {
	return internalLCG_C;
}

/**
 * @brief Getter for the mod
 * 
 * @return Mod
 */
int LCGGetMod(void) {
	return internalLCG_M;
}
