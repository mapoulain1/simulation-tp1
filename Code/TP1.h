#ifndef TP1_H_
#define TP1_H_

/**
 * @brief Roll a custom dice N times
 * 
 * @param faces Number of faces of the dice
 * @param numberOfTry Number of times to roll
 */
void rollTheDice(int faces, int numberOfTry);

/**
 * @brief Toss a coin N times
 * 
 * @param numberOfTry Number of times to toss
 */
void tossingCoin(int numberOfTry);

void testMiddleSqure(void);

void testTossingCoin(void);

void testRollingDice(void);

void testLCG(void);

void testFloatRand(void);

/**
 * @brief Entry point
 * 
 * @param argc 
 * @param argv 
 * @return int 
 */
int main(int argc, char const *argv[]);

#endif